﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour {

    [Header("Damage Settings")]
    public int DamageAmount;
}
