﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_Projectile : WeaponController
{
    public Projectile projectile;
    public float projectileSpeed;
    public float RotationOffset = 0f;

    protected override void Start()
    {
        //custom stuff
        base.Start();
    }

    protected override void Fire()
    {
        //TODO: get projectile from pool
        var bullet = Instantiate(projectile, firePoint.transform.position, firePoint.transform.rotation);
        //Projectile bullet = ObjectPooler.SharedInstance.GetPooledObject("PlayerBullet").GetComponent<Projectile>();
        
        if (bullet)
        {
            bullet.transform.SetPositionAndRotation(firePoint.position, firePoint.rotation);
            //deviation
            //var rot = bullet.transform.rotation.eulerAngles;
            //var angleOfDeviation = Random.Range(-deviation, deviation);
            //rot.z = rot.z + angleOfDeviation + RotationOffset;
            //bullet.transform.rotation = Quaternion.Euler(rot);

            bullet .SetValues(damage, range, projectileSpeed, isPassthrough);
            bullet.Launch();
            GetComponent<SoundEffect>().Play();
            base.Fire();
        }
        else
        {
            Debug.Log("Error getting bullet");
        }
    }
}
