﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{

    [Header("Health Settings")]
    public int MaxHealth = 10;
    public bool IsInvulnerable = false;
    public int _currentHealth;
    public Slider HealthSlider;
    [Header("Collision Damage")]
    public bool TakeDamageOnCollision = false;
    public LayerMask CollisionLayer;
    public int AmountOfDamageOnCollision = 1;

    public GameObject GameOverPanel;

    //Public Properties
    public bool IsAlive { get; set; }

    //private variables
    

    // Use this for initialization
    void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        _currentHealth = MaxHealth;
        //HealthSlider = GetComponent<Slider>();
        //GameOverPanel = GetComponent<GameObject>();
        IsAlive = true;
    }

    //public virtual void TakeDamage(int _damageAmount)
    //{
    //    Debug.Log("Damage" + _damageAmount);
    //    _currentHealth -= _damageAmount;
    //    CheckHealth();
    //}

    public virtual IEnumerator TakeDamage(int _damageAmount)
    {

        _currentHealth -= _damageAmount;
        CheckHealth();
        yield return new WaitForSeconds(0.1f);
    }

    protected virtual void CheckHealth()
    {
        if (gameObject.tag == "Player")
        {
            HealthSlider.value = (float)_currentHealth / MaxHealth;
        }
        if (_currentHealth <= 0)
        {
            IsAlive = false;
            gameObject.SetActive(false);
            GameManager.Instance.score += GameManager.Instance.waveCount + 10;
            GameObject scrap = ObjectPoolManager.Instance.GetObjectFromPool("Scrap");
            scrap.transform.position = transform.position;
            scrap.SetActive(true);
            if (gameObject.tag == "Player")
            {
                GameOverPanel.SetActive(true);
            }
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (!IsInvulnerable && collision.gameObject.tag != "Enemy")
        {
            Damage damage = collision.GetComponent<Damage>();
            if (damage != null)
            {
                StartCoroutine(TakeDamage(damage.DamageAmount)); 
            }
            else
            {
                StartCoroutine(TakeDamage(AmountOfDamageOnCollision));
            }
        }
    }
}
