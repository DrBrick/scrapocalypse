﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    public GameObject GameOverPanel;
	// Use this for initialization
	void OnEnable () {
        Time.timeScale = 0;
	}
	
    public void RestartLevel()
    {
        GameManager.Instance.waveCount = 0;
        GameManager.Instance.score = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitLevel()
    {
        GameManager.Instance.waveCount = 0;
        GameManager.Instance.score = 0;
        SceneManager.LoadScene(0);
    }
}
