﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleUI : MonoBehaviour {

    public GameObject mainMenu;
    public GameObject optionsMenu;
    public Slider volumeSlider;
    public Slider soundSlider;

	// Use this for initialization
	void Awake () {
        AudioManager.Instance.SetMusicVolume(volumeSlider.value);
        AudioManager.Instance.SetSoundVolume(soundSlider.value);
        mainMenu.SetActive(true);
        optionsMenu.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPlay()
    {
        SceneManager.LoadScene("HordeMode");
    }

    public void Options()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
    }

    public void OnCloseOptions()
    {
        optionsMenu.SetActive(false);
        mainMenu.SetActive(true);
    }
    
    public void OnVolumeChange()
    {
        AudioManager.Instance.SetMusicVolume(volumeSlider.value);
    }

    public void OnSoundChange()
    {
        AudioManager.Instance.SetSoundVolume(soundSlider.value);
    }
}
