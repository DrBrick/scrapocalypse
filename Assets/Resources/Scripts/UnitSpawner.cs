﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSpawner : Singleton<UnitSpawner> {

    [Header("Spawn Settings")]
    public float SpawnWaveTime;
    public BoxCollider2D SpawnArea;

    //Private variables


	// Use this for initialization
	void Start () {
        Init();
	}

    protected override void Init()
    {
        SpawnArea = GetComponent<BoxCollider2D>();
        StartCoroutine("SpawnWaves");
    }
	
    public IEnumerable SpawnWaves()
    {
        Debug.Log("Spawning");
        while(true)
        {
            GameObject _enemy = ObjectPooler.Instance.GetPooledObject("Robot");
            Debug.Log(_enemy);
            _enemy.transform.position = GetRandomPosition();
            Debug.Log(_enemy.transform.position);
            _enemy.SetActive(true);
            yield return new WaitForSeconds(SpawnWaveTime);
        }
        
    }

    public void StartSpawner()
    {
        StartCoroutine("SpawnWaves");
    }

    public void StopSpawner()
    {
        StopCoroutine("SpawnWaves");
    }

    public Vector2 GetRandomPosition()
    {
        return new Vector2(Random.Range(-SpawnArea.size.x, SpawnArea.size.x), Random.Range(-SpawnArea.size.y, SpawnArea.size.y));
    }
}
