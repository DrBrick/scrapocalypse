﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpoilsScript2D : MonoBehaviour {

    public GameObject[] spoilPrefabs;
    public int spoilCount;
    public float spoilRadius;
    private List<GameObject> spoilObjects;

    // Use this for initialization
    void Start () {
        spoilObjects = new List<GameObject>();
		foreach(GameObject spoil in spoilPrefabs)
        {
            for(int i = 0; i < spoilCount; i++)
            {
                GameObject newObj = Instantiate(spoil);
                newObj.SetActive(false);
                spoilObjects.Add(newObj);
            }
        }
	}
	[ContextMenu("Spread")]
    public void SpreadSpoils()
    {
        foreach(GameObject spoil in spoilObjects)
        {
            spoil.transform.position = new Vector3(Random.Range(-spoilRadius, spoilRadius), Random.Range(-spoilRadius, spoilRadius), transform.position.x);
            spoil.SetActive(true);
        }
    }
}
