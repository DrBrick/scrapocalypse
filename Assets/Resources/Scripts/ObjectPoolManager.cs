﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour {

    public static List<ObjectPool> ObjectPools;

    public static ObjectPoolManager Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        DontDestroyOnLoad(this);
    }

    // Use this for initialization
    void Start () {
        Init();
	}

     private void Init()
    {
        ObjectPools = new List<ObjectPool>();
    }


    public void CreateObjectPool(PoolItem _poolItem)
    {
        ObjectPool objPool = ScriptableObject.CreateInstance<ObjectPool>();
        objPool.CreateObjectPool(_poolItem);
        ObjectPools.Add(objPool);
    }


    public GameObject GetObjectFromPool(GameObject gameObject)
    {
        foreach(ObjectPool objPool in ObjectPools)
        {
            if(objPool.name == gameObject.name)
            {
                return objPool.GetPooledObject();
            }
        }

        return null;
    }

    public GameObject GetObjectFromPool(string tag)
    {
        foreach (ObjectPool objPool in ObjectPools)
        {
            if (objPool.PoolItem.PoolObject.name == tag)
            {
                return objPool.GetPooledObject();
            }
        }
        return null;
    }
}
