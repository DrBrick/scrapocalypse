﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolItem {

    public GameObject PoolObject;
    public int PoolAmount;
    public bool CanGrow;

    public PoolItem()
    {
        
    }

    public PoolItem(GameObject _poolObject, int _poolAmount, bool _canGrow)
    {
        PoolObject = _poolObject;
        PoolAmount = _poolAmount;
        CanGrow = _canGrow;
    }
}
