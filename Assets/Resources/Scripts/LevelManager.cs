﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {


    public Transform Player;
    public static LevelManager Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    // Use this for initialization
    void Start () {
        Initialize();
    }

    private void Initialize()
    {
        Instance.Player = FindObjectOfType<Player>().transform;
    }
}
