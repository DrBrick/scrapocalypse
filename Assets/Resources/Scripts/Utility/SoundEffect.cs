﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffect : MonoBehaviour {

    [Range(0.0f,1.0f)]
    public float volume =  0.5f;
    public AudioClip audioClip;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Play()
    {
        AudioManager.Instance.PlaySound(audioClip, volume);
    }
}
