﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Projectile : MonoBehaviour
{
    public float speed;
    public float range;
    public bool isPassThrough = false;
    public bool destroyOnImpact = false;

    public AudioSource audioSource;
    public AudioClip launchAudio;
    public AudioClip travelAudio;
    public AudioClip impactAudio;

    private Rigidbody2D rb;

    private int damage;

    Vector2 movement;
    Vector2 startPos;
    Vector2 targetPos;

    public void SetValues(int? damage = null, float? range = null, float? speed = null,
        bool? isPassThrough = null)
    {
        if (damage != null)
            this.damage = (int)damage;
        if (range != null)
            this.range = (float)range;
        if (speed != null)
            this.speed = (float)speed;
        if (isPassThrough != null)
            this.isPassThrough = (bool)isPassThrough;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //gameObject.SetActive(false);
    }

    void FixedUpdate()
    {
        Move();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            return;
        }

        if (destroyOnImpact || !isPassThrough)
        {
            //Debug.Log("Collision: disabling");
            gameObject.SetActive(false);
        }

        Damageable damageable = collision.gameObject.GetComponent<Damageable>();

        if (damageable)
        {
            damageable.TakeDamage(damage);
        }
    }

    public void Launch()
    {
        //Debug.Log("Launch");
        //TODO: make sure this is happening at the fire location, and not wherever the object gets pooled to
        startPos = transform.position;
        targetPos = startPos + (Vector2)(transform.right * range);
        gameObject.SetActive(true);
    }

    void Move()
    {
        movement = transform.right * speed * Time.deltaTime;

        rb.MovePosition((Vector2)transform.position + movement);

        //TODO: allow custom logic. eg allow grenade to stop and sit at target location
        if ((Vector2)transform.position == targetPos)
        {
            //Debug.Log("Projectile reached target destination");
            gameObject.SetActive(false);
        }
        if (Vector3.Distance(startPos, transform.position) > Vector3.Distance(startPos, targetPos))
        {
            //Debug.Log("We have gone too far");
            gameObject.SetActive(false);
        }
    }
}
