﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponController : MonoBehaviour
{
    #region Properties
    /// <summary>
    /// The origin point for the attack/bullet.
    /// </summary>
    public Transform firePoint;
    /// <summary>
    /// Pre-mitigated damage to apply to a Damageable object.
    /// </summary>
    public int damage;
    /// <summary>
    /// Number of shots that can be fired before reloading. 
    /// </summary>
    public int clipSize;
    public Text clipAmmoText;
    public int ammoPerShot = 1;
    public int reserveAmmoCapacity;
    public Text reserveAmmoText;
    public float range;
    /// <summary>
    /// Time in seconds before another round can be fired.
    /// </summary>
    public float fireDelay;
    /// <summary>
    /// Time in seconds taken to reload the weapon.
    /// </summary>
    public float reloadTime;
    /// <summary>
    /// True = Automatic, False = Semiautomatic
    /// </summary>
    public bool isAutomatic;
    /// <summary>
    /// True for melee weapons that don't require ammo and can still "fire" at 0.
    /// Also allows melee weapons to have ammo/charges.
    /// </summary>
    public bool isMelee;
    /// <summary>
    /// True: The shot passes through other colliders. False: The shot stops on colliders.
    /// </summary>
    public bool isPassthrough = false;
    /// <summary>
    /// Number of individual rounds fired per trigger pull.
    /// </summary>
    public int burstCount = 1;
    /// <summary>
    /// Time in seconds between each shot in a burst.
    /// </summary>
    public float burstDelay;
    /// <summary>
    /// Number of shots fired per bullet consumed. Think shotgun, or Warframe-style multishot.
    /// Will fire one shot per real number. Decimals add a percent chance for another.
    /// </summary>
    public float multiShot = 1;

    #region New Additions: Items need logic added. Remove from region when functional.

    public LayerMask layerMask;

    /// <summary>
    /// Accuracy/deviation potential in degrees of any given firing of the weapon. 0 is pinpoint accurate.
    /// 90 covers a full 180 degree spread from the fire point and should proably never be used
    /// for anything other than laughs.
    /// </summary>
    //[Range(0, 90)]
    //public float deviation;

    /// <summary>
    /// Size of multishot spread. At 0, each "piece" of the shot will follow the same path.
    /// </summary>
    //public float spread;

    //public float recoil;
    public bool isUnlimitedAmmo;
    public AudioSource audioSource;
    public AudioClip fireAudio;
    public AudioClip reloadAudio;
    public Light fireLight;
    public float fireLightDuration;

    #endregion

    protected int ammoInClip;
    protected int ammoInReserve;
    protected bool isFireButtonDown = false;
    /// <summary>
    /// Weapon has fired since button was pressed.
    /// </summary>
    protected bool isFired = false;
    protected bool isFireDelayed = false;
    protected bool isReloading = false;
    #endregion

    protected virtual void Start()
    {
        //TODO: remove/change below as needed
        ammoInClip = clipSize;
        ammoInReserve = reserveAmmoCapacity;

        //clipAmmoText = GameObject.Find("ClipAmmoText").GetComponent<Text>();
        //reserveAmmoText = GameObject.Find("ReserveAmmoText").GetComponent<Text>();

        UpdateAmmoText();

        audioSource = GetComponent<AudioSource>();

        if(firePoint == null)
        {
            firePoint = transform.Find("FirePoint");
            //Debug.Log("Firepoint: " + firePoint);
        }
    }

    void Update()
    {
        //rotate towards mouse
        
        //

        if (isFireButtonDown && CanFire())
        {
            StartCoroutine(FireWeapon());
        }
    }

    public void OnFireButtonDown()
    {
        isFireButtonDown = true;
    }

    public void OnFireButtonUp()
    {
        isFireButtonDown = false;
        if (!isFireDelayed)
        {
            isFired = false;
        }
    }

    /// <summary>
    /// Fires the weapon. Must include custom logic in overrides then call base.Fire().
    /// </summary>
    protected virtual void Fire()
    {

    }

    private IEnumerator FireWeapon()
    {
        isFired = true;

        for (int i = 1; i <= burstCount; i++)
        {
            if (ammoInClip <= 0 && clipSize != 0)
            {
                yield break;
            }
            DeductAmmo();
            if (audioSource && fireAudio)
            {
                audioSource.PlayOneShot(fireAudio);
            }
            StartCoroutine(MuzzleFlash());

            //multishot
            float chanceOfExtraBullet = multiShot % 1;
            int bulletsToFire = (int)(multiShot - chanceOfExtraBullet);
            if (Random.value <= chanceOfExtraBullet)
            {
                bulletsToFire++;
            }

            for (int j = 1; j <= bulletsToFire; j++)
            {
                Fire();
            }

            UpdateAmmoText();
            //
            if(i == burstCount)
            {
                continue;
            }
            isFireDelayed = true;
            yield return new WaitForSeconds(burstDelay);
        }
        //StartCoroutine(FireDelay(fireDelay));
        isFireDelayed = true;
        yield return new WaitForSeconds(fireDelay);
        isFireDelayed = false;
        if (!isFireButtonDown)
        {
            isFired = false;
        }
    }

    private IEnumerator MuzzleFlash()
    {
        if (fireLight == null)
        {
            yield break;
        }
        fireLight.enabled = true;
        yield return new WaitForSeconds(fireLightDuration);
        fireLight.enabled = false;
    }

    private void DeductAmmo()
    {
        if (ammoInClip >= ammoPerShot && !isUnlimitedAmmo)
        {
            ammoInClip -= ammoPerShot;
        }
    }

    private IEnumerator FireDelay(float delay)
    {
        isFireDelayed = true;
        yield return new WaitForSeconds(delay);
        isFireDelayed = false;
    }

    public void OnReloadButtonDown()
    {
        StartCoroutine(Reload());
    }

    private IEnumerator Reload()
    {
        //Do not reload if: reserve ammo cap == 0 (unlimited ammo)
        //                  clip is full
        //                  reserves are empty
        if ((reserveAmmoCapacity != 0) && (ammoInClip == clipSize || ammoInReserve == 0))
        {
            yield break;
        }

        isReloading = true;

        if (audioSource && reloadAudio)
        {
            audioSource.PlayOneShot(reloadAudio);
        }

        yield return new WaitForSeconds(reloadTime);

        int needed = clipSize - ammoInClip;
        //take needed ammo from reserve if there's enough
        if (ammoInReserve >= needed)
        {
            ammoInClip += needed;
            ammoInReserve -= needed;
        }
        //unlimited ammo, with clip size. refill clip
        else if (reserveAmmoCapacity == 0)
        {
            ammoInClip += needed;
        }
        //pull any ammo that is available
        else
        {
            ammoInClip += ammoInReserve;
            ammoInReserve -= ammoInReserve;
        }
        UpdateAmmoText();
        isReloading = false;
    }

    /// <summary>
    /// Determines whether the weapon can be "fired."
    /// </summary>
    /// <returns>True if melee or has available ammo, else False.</returns>
    private bool CanFire()
    {
        bool canFire = false;

        if ((isMelee || clipSize == 0 || ammoInClip >= ammoPerShot)
            && (isReloading == false && isFireDelayed == false))
        {
            if ((!isAutomatic && isFired) || isFireDelayed)
            {
                canFire = false;
            }
            else
            {
                canFire = true;
            }
        }

        return canFire;
    }

    protected void Damage(Damageable target)
    {
        target.TakeDamage(damage);
    }

    private void UpdateAmmoText()
    {
        if (clipAmmoText)
        {
            clipAmmoText.text = ammoInClip + "/" + clipSize;
        }

        if (reserveAmmoText)
        {
            if (reserveAmmoCapacity == 0)
            {
                //reserveAmmoText.fontSize = 30;
                reserveAmmoText.text = "\u221E";
            }
            else
            {
                //reserveAmmoText.fontSize = 20;
                reserveAmmoText.text = ammoInReserve.ToString();
            }
        }
    }

}

