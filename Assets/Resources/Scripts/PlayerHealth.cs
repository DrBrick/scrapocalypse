﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : Health
{

    public Slider healthSlider;

    // Use this for initialization
    void Start()
    {
        healthSlider = GetComponent<Slider>();
    }

    private void Update()
    {
        healthSlider.value = _currentHealth;
    }
}
