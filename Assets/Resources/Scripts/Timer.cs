﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public float duration;
    public bool isContinuous;
    public Image radialGauge;
    public Text timerText;

    private float startTime;
    private float pauseTimeStart;
    private float pauseDuration;
    private bool isTick;
    private bool isPaused;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(!isTick)
        {
            UpdateRadialGauge();
            timerText.text = (duration - (Time.time + pauseDuration - startTime)).ToString("0");
            if (Time.time + pauseDuration >= startTime + duration)
            {
                isTick = true;
            }
        }

        
    }

    public void StartTimer()
    {
        startTime = Time.time;
    }

    public void Pause()
    {
        if (!isPaused)
        {
            isPaused = true;
            pauseTimeStart = Time.time;
        }
    }

    public void Resume()
    {
        if (isPaused)
        {
            isPaused = false;
            pauseDuration += Time.time - pauseTimeStart;
        }
    }

    public void Reset()
    {
        isTick = false;
        startTime = 0.0f;
        pauseTimeStart = 0.0f;
        pauseDuration = 0.0f;
    }

    public bool IsTick()
    {
        bool result = false;
       if(isTick)
        {
            if(isContinuous)
            {
                startTime = Time.time;
                pauseTimeStart = 0.0f;
                pauseDuration = 0.0f;
                isTick = false;
                
            }
            result = true;
        }
        return result;
    }

    private void UpdateRadialGauge()
    {
        radialGauge.fillAmount = 1- (Time.time + pauseDuration - startTime) / duration;
    }
}
