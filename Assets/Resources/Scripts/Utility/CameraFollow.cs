﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float smoothing = 5f;

    //Vector2 offset;
    Vector3 offset;

    void Start()
    {
        offset = transform.position - target.position;
    }

    void FixedUpdate()
    {
        //Vector2 targetCamPos = (Vector2)target.position + offset;
        Vector3 targetCamPos = target.position + offset;
        targetCamPos.z = -10;

        //transform.position = Vector2.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    }
}
