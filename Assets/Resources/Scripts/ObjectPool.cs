﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : ScriptableObject{

    //public GameObject PoolObject;
    //public int PoolAmount;
    //public bool CanGrow;
    public PoolItem PoolItem;
    public List<GameObject> pooledObjects;

    public ObjectPool(PoolItem _poolItem)
    {
        PoolItem = new PoolItem();
        PoolItem.PoolObject = _poolItem.PoolObject;
        PoolItem.PoolAmount = _poolItem.PoolAmount;
        PoolItem.CanGrow = _poolItem.CanGrow;
        CreateObjectPool();
    }

    public ObjectPool(GameObject _poolObject, int _poolAmount, bool _canGrow)
    {
        PoolItem = new PoolItem();
        PoolItem.PoolObject = _poolObject;
        PoolItem.PoolAmount = _poolAmount;
        PoolItem.CanGrow = _canGrow;
        CreateObjectPool();
    }

    public void CreateObjectPool(PoolItem _poolItem)
    {
        PoolItem = _poolItem;

        pooledObjects = new List<GameObject>();
        for (int i = 0; i < PoolItem.PoolAmount; i++)
        {
            GameObject newObj = Instantiate(PoolItem.PoolObject);
            newObj.SetActive(false);
            pooledObjects.Add(newObj);
        }
    }

    public void CreateObjectPool(GameObject _poolObject, int _poolAmount, bool _canGrow)
    {
        PoolItem = new PoolItem();
        PoolItem.PoolObject = _poolObject;
        PoolItem.PoolAmount = _poolAmount;
        PoolItem.CanGrow = _canGrow;

        pooledObjects = new List<GameObject>();
        for (int i = 0; i < PoolItem.PoolAmount; i++)
        {
            GameObject newObj = Instantiate(PoolItem.PoolObject);
            newObj.SetActive(false);
            pooledObjects.Add(newObj);
        }
    }

    public void CreateObjectPool()
    {
        pooledObjects = new List<GameObject>();
        for(int i = 0; i < PoolItem.PoolAmount; i++)
        {
            GameObject newObj = Instantiate(PoolItem.PoolObject);
            newObj.SetActive(false);
            pooledObjects.Add(newObj);
        }
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }
        }

        if (PoolItem.CanGrow)
        {
            GameObject obj = Instantiate(PoolItem.PoolObject);
            obj.SetActive(false);
            pooledObjects.Add(obj);
            return obj;
        }
        return null;
    }
}
