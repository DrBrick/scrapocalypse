﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{

    //Public Variables
    [Header("AI Settings")]
    public float Speed;
    public float StopDistance;
    public float SightDistance = 1f;
    public float RotationOffset;
    public LayerMask SightLayer;
    public EnemyType EnemyType;
    public int DamageAmount = 0;

    //Private variables
    private Rigidbody2D _rb;
    private Health _health;
    private bool _isObstructed;
    private Animator _anim;
    private bool _canAttack = false;
    // Use this for initialization
    void Start()
    {
        Initialize();
    }

    protected virtual void Initialize()
    {
        _rb = GetComponent<Rigidbody2D>();
        _health = GetComponent<Health>();
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(_health.IsAlive)
        {
            CheckForObstacles();
            MoveTowardPlayer();
            Attack();
        }
    }

    protected virtual void CheckForObstacles()
    {
        _isObstructed =  CastRays();
    }

    protected virtual bool CastRays()
    {
        RaycastHit2D _midHit = Physics2D.Raycast(transform.position, transform.forward, SightDistance, SightLayer);
        Debug.DrawRay(transform.position, transform.up, Color.red);
        RaycastHit2D _leftHit = Physics2D.Raycast(transform.position, -transform.right, SightDistance, SightLayer);
        Debug.DrawRay(transform.position, new Vector2(-transform.right.x, -transform.right.y), Color.red);
        RaycastHit2D _rightHit = Physics2D.Raycast(transform.position, transform.right, SightDistance, SightLayer);
        Debug.DrawRay(transform.position, new Vector2(transform.right.x, transform.right.y), Color.red);
        return _midHit || _leftHit || _rightHit;
    }

    protected virtual void MoveTowardPlayer()
    {
        if (_isObstructed)
        {
            return;
        }
        _rb.velocity = Vector2.zero;
        Vector3 _playerPos = LevelManager.Instance.Player.transform.position;
        Vector2 _direction = (_playerPos - transform.position).normalized;
        float _distance = Vector2.Distance(_playerPos, transform.position);
        float _angle = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg + RotationOffset;
        Quaternion _newRotation = Quaternion.AngleAxis(_angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, _newRotation, Time.deltaTime * Speed);
        if ( _distance <= StopDistance)
        {
            _rb.velocity = Vector2.zero;
            _canAttack = true;
        }
        else 
        {
            _rb.AddForce(_direction * Speed, ForceMode2D.Force);
            _canAttack = false;
        }
    }

    private float lastAttackTime;
    public float attackDuration;

    protected virtual void Attack()
    {

        if(_canAttack)
        {
            if (Time.time > lastAttackTime + attackDuration)
            {
                lastAttackTime = Time.time;
                if (_anim != null)
                {
                    //_anim.SetBool("Attack", _canAttack);
                    _anim.SetTrigger("Attack");
                }
                RaycastHit2D _hit = Physics2D.Raycast(transform.position, transform.up, SightDistance, SightLayer);
                Debug.DrawRay(transform.position, transform.up, Color.cyan);
                Debug.Log(_hit);
                if (_hit)
                {
                    Health _health = _hit.collider.gameObject.GetComponent<Health>();
                    if (_health != null)
                    {
                        //_health.TakeDamage(DamageAmount);
                        StartCoroutine(_health.TakeDamage(DamageAmount));
                    }

                }
                if (EnemyType == EnemyType.Range)
                {

                    GameObject bullet = ObjectPoolManager.Instance.GetObjectFromPool("Botbullet");
                    bullet.transform.position = transform.position;
                    bullet.transform.rotation = transform.rotation;
                    bullet.SetActive(true);
                }
            }
        }

    }
}

