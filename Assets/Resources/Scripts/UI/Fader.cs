﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{

    private SpriteRenderer[] spriteRenderers;
    private CanvasRenderer[] canvasRenderers;
    //private Text[] texts;

    public float duration = 1.0f;
    public float cycleDelay = 1.0f;

    private float startTime = 0.0f;
    public bool isFade = false;
    private bool isRunning = false;
    private bool isCycleShow = false;

    // Use this for initialization
    void Awake()
    {
        spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
        canvasRenderers = GetComponentsInChildren<CanvasRenderer>();
        //texts = GetComponentsInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isRunning)
        {
            if (Time.time <= startTime + duration)
            {
                float normalizedTime = (Time.time - startTime) / duration;
                if (isFade)
                {
                    normalizedTime = 1 - normalizedTime;
                }
                UpdateColors(normalizedTime);
            }
            else
            {
                if (isCycleShow)
                {
                    if (isFade)
                    {
                        isRunning = false;
                        UpdateColors(0.0f);
                    }
                    else
                    {
                        if (Time.time >= startTime + duration + cycleDelay)
                        {
                            Fade();
                        }
                    }
                }
                else
                {
                    if (isFade)
                    {
                        UpdateColors(0.0f);
                    }
                    else
                    {
                        UpdateColors(1.0f);
                    }
                }
            }

        }
    }

    private void UpdateColors(float alpha)
    {
        foreach (SpriteRenderer renderer in spriteRenderers)
        {
            Color color = renderer.color;
            Color newColor = new Color(color.r, color.g, color.b, alpha);
            renderer.color = newColor;
        }

        foreach (CanvasRenderer renderer in canvasRenderers)
        {
            renderer.SetAlpha(alpha);
        }

        //foreach(Text text in texts)
        //{
        //    Color color = text.color;
        //    Color newColor = new Color(color.r, color.g, color.b, alpha);
        //    text.color = newColor;
        //}
    }

    public void Fade()
    {
        isRunning = true;
        startTime = Time.time;
        isFade = true;
    }

    public void Show()
    {
        isRunning = true;
        startTime = Time.time;
        isFade = false;
    }

    public void CycleShow()
    {
        isCycleShow = true;
        Show();
    }

    public void SetAlphaAll(float alpha)
    {
        UpdateColors(alpha);
    }

    public static void SetAplhaAll(GameObject go, float alpha)
    {
        SpriteRenderer[] sr = go.GetComponentsInChildren<SpriteRenderer>();
        CanvasRenderer[] cr = go.GetComponentsInChildren<CanvasRenderer>();
        foreach (SpriteRenderer renderer in sr)
        {
            Color color = renderer.color;
            Color newColor = new Color(color.r, color.g, color.b, alpha);
            renderer.color = newColor;
        }

        foreach (CanvasRenderer renderer in cr)
        {
            renderer.SetAlpha(alpha);
        }
    }
}

