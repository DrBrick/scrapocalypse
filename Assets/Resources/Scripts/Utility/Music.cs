﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour {

    public string introMusicName;
    public string loopMusicName;

    public bool isFirstRun;
    private float startTime;

	// Use this for initialization
	void Start () {
        isFirstRun = true;
        startTime = Time.time;
        AudioManager.Instance.PlayMusic(introMusicName);
        
    }
	
	// Update is called once per frame
	void Update () {
		if(isFirstRun)
        {
            if (Time.time >= AudioManager.Instance.GetMusicLength + startTime)
            {
                isFirstRun = false;
                AudioManager.Instance.PlayMusic(loopMusicName);
            }

        }
	}


}
