﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{

    static T instance;

    public static T Instance
    {
        get
        {
            MakeSingletone();

            return instance;
        }
        set
        {
            instance = value;
        }
    }

    void Start()
    {
        MakeSingletone();
    }

    static void MakeSingletone()
    {
        if (instance == null)
        {
            GameObject foo = new GameObject();
            foo.name = typeof(T).ToString();
            instance = foo.AddComponent<T>();
            instance.SendMessage("Init", SendMessageOptions.DontRequireReceiver);
            DontDestroyOnLoad(foo);
        }
    }

    protected abstract void Init();
}
