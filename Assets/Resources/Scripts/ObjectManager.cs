﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager : MonoBehaviour {

    public List<GameObject> ObjectsToPool;

    private void Start()
    {
        Init();   
    }

    public void Init()
    {
        foreach (GameObject gameObject in ObjectsToPool)
        {
            ObjectPoolManager.Instance.CreateObjectPool(new PoolItem(gameObject, 1, true));
        }
    }

    //public List<PoolItem> Enemies;
    //public List<PoolItem> Bullets;
    //public List<PoolItem> Scrap;

    //private List<List<PoolItem>> ObjectsToPool;

    //private void Start()
    //{
    //    Init();
    //}

    //protected virtual void Init()
    //{
    //    ObjectsToPool = new List<List<PoolItem>>();
    //    Enemies = new List<PoolItem>();
    //    Bullets = new List<PoolItem>();
    //    Scrap = new List<PoolItem>();
    //    ObjectsToPool.Add(Enemies);
    //    ObjectsToPool.Add(Bullets);
    //    ObjectsToPool.Add(Scrap);
    //    PoolAllObjects();
    //}

    //protected virtual void PoolAllObjects()
    //{
    //    foreach (List<PoolItem> objectPool in ObjectsToPool)
    //    {
    //        foreach (PoolItem poolItem in objectPool)
    //        {
    //            ObjectPoolManager.Instance.CreateObjectPool(poolItem);
    //        }
    //    }
    //}
}
