﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent(typeof(PlayerInput))]
public class Player : MonoBehaviour
{
    [Header("Weapon")]
    public WeaponController Weapon;
    
    //[Header("Inventory")]
    //public Inventory Inventory;

    [Header("Movement Settings")]
    public float walkSpeed = 6;
    public float runSpeed = 10;

    //Private fields
    private bool _isRunning = false;
    private float _accelerationTimeGrounded = .1f;

    private Vector2 _movement;                   // The vector to store the direction of the player's movement.
    private Animator _anim;                      // Reference to the animator component.
    private Rigidbody2D _playerRigidbody;          // Reference to the player's rigidbody.
    private int _floorMask;                      // A layer mask so that a ray can be cast just at gameobjects on the floor layer.
    private float _camRayLength = 100f;          // The length of the ray from the camera into the scene.
    //

    void Awake()
    {
        // Create a layer mask for the floor layer.
        //floorMask = LayerMask.GetMask("Floor");
        Time.timeScale = 1;
        // Set up references.
        _anim = GetComponent<Animator>();
        _playerRigidbody = GetComponent<Rigidbody2D>();

        if (Weapon == null)
        {
            Weapon = GetComponentInChildren<WeaponController>();
        }
    }

    void FixedUpdate()
    {
        _playerRigidbody.MovePosition((Vector2)transform.position + _movement);

    }

    public void DirectionalInput(float h, float v)
    {
        // Set the movement vector based on the axis input.
        _movement.Set(h, v);
        Animating(h, v);

        // Normalise the movement vector and make it proportional to the speed per second.
        //movement = movement.normalized * speed * Time.deltaTime;
        _movement = _movement.normalized * Time.deltaTime * (_isRunning ? runSpeed : walkSpeed);
    }

    public void Animating(float h, float v)
    {
        // Create a boolean that is true if either of the input axes is non-zero.
        bool walking = (h != 0f || v != 0f);
        // Tell the animator whether or not the player is walking.
        _anim.SetBool("IsWalking", walking);
    }

    //public void OnRunInputDown()
    //{
    //    _isRunning = true;
    //}

    //public void OnRunInputUp()
    //{
    //    _isRunning = false;
    //}

    public void OnFireButtonDown()
    {
        if (Weapon != null)
        {
            Weapon.OnFireButtonDown();
            _anim.SetTrigger("FireWeapon");
        }
        else
        {
            Debug.Log("No weapon equipped");
        }
    }

    public void OnFireButtonUp()
    {
        if (Weapon != null)
        {
            Weapon.OnFireButtonUp();
        }
    }

    //public void OnReloadButtonDown()
    //{
    //    if (Weapon != null)
    //    {
    //        Weapon.OnReloadButtonDown();
    //    }
    //}
}