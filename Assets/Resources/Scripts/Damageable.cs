﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Damageable : MonoBehaviour
{
    public int maxHealth;
    [SerializeField]
    private int currentHealth;

    /// <summary>
    /// Duration of invulnerability after taking damage.
    /// </summary>
    public float invulnerabilityPeriod;
    public bool isInvulnerable = false;

    #region InDevelopment
    public Slider healthSlider;
    public Slider damageSlider;
    private bool isDamageSliderDecaying;

    public int shield;

    public float defense;

    public bool canRegen;
    public float regenAmount;
    public float regenRate;
    #endregion

    void Awake()
    {
        currentHealth = maxHealth;

        if (healthSlider)
        {
            healthSlider.maxValue = maxHealth;
            healthSlider.minValue = 0;
        }
        if (damageSlider)
        {
            damageSlider.maxValue = maxHealth;
            damageSlider.minValue = 0;
            damageSlider.value = currentHealth;
        }
    }

    void Start()
    {

        UpdateHealthUI();
    }

    void Update()
    {
        //if (damageSlider.value > healthSlider.value)
        if(isDamageSliderDecaying)
        {
            //damageSlider.value = Mathf.Lerp(damageSlider.value, healthSlider.value, Time.deltaTime * 3);
            //monster.position += (player.position - monster.position).normalized * speed * Time.deltaTime
            damageSlider.value += (healthSlider.value - damageSlider.value) * 3 * Time.deltaTime;

            if (damageSlider.value <= healthSlider.value + 0.015)
            {
                damageSlider.value = healthSlider.value;
                isDamageSliderDecaying = false;
            }
        }
    }

    public void TakeDamage(int damage)
    {
        if (!isInvulnerable)
        {
            currentHealth -= damage;
            UpdateHealthUI();
            if (currentHealth <= 0)
            {
                Die();
                return;
            }
            if (invulnerabilityPeriod != 0)
            {
                StartCoroutine(InvulnerabilityPeriod());
            }
        }
    }

    public void Heal(int heal, bool overHeal = false)
    {
        int missingHealth = maxHealth - currentHealth;
        if (heal <= missingHealth || overHeal)
        {
            currentHealth += heal;
        }
        else
        {
            currentHealth += missingHealth;
        }
        UpdateHealthUI();
    }

    private IEnumerator InvulnerabilityPeriod()
    {
        //Debug.Log("Begin invulnerability: " + gameObject.name);
        isInvulnerable = true;
        yield return new WaitForSeconds(invulnerabilityPeriod);
        //Debug.Log("End invulnerability: " + gameObject.name);
        isInvulnerable = false;
    }

    void Die()
    {
        //Debug.Log(gameObject.name + " died.");
        gameObject.SetActive(false);
    }

    private void UpdateHealthUI()
    {
        if (!healthSlider)
        {
            return;
        }
        healthSlider.maxValue = maxHealth; //+ shield; //not yet implemented
        if (damageSlider)
        {
            StartCoroutine(DecayDamageSlider());
        }
        healthSlider.value = currentHealth;
    }

    private IEnumerator DecayDamageSlider()
    {
        isDamageSliderDecaying = false;
        yield return new WaitForSeconds(1);
        //damageSlider.value = currentHealth;
        isDamageSliderDecaying = true;
    }
}
