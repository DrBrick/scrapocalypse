﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    private float GlobalVolume = 1.0f;
    private float MusicVolume = 1.0f;
    private float SoundVolume = 1.0f;

    private bool isMusicMuted;
    private bool isSoundMuted;
    // Use this to mute game during production
    public bool mute;

    public bool IsMusicMuted
    {
        get { return isMusicMuted; }
    }

    public bool IsSoundMuted
    {
        get { return isSoundMuted; }
    }

    public bool IsMusicPlaying
    {
        get { return musicChannel.isPlaying; }
    }

    public float GetMusicLength
    {
        get { return musicChannel.clip.length; }
    }

    private AudioSource musicChannel;
    private AudioSource soundChannel;
    private bool audioMute = false;
    private Dictionary<string, AudioClip> soundMap;
    private bool musicDelayOverride;

    protected override void Init()
    {
        soundMap = new Dictionary<string, AudioClip>();

        musicChannel = new GameObject().AddComponent<AudioSource>();
        musicChannel.transform.SetParent(transform);
        musicChannel.name = "MusicChannel";
        musicChannel.tag = "MusicChannel";
        musicChannel.loop = true;
        soundChannel = new GameObject().AddComponent<AudioSource>();
        soundChannel.transform.SetParent(transform);
        soundChannel.name = "SoundChannel";
        soundChannel.tag = "SoundChannel";

        AudioClip[] clips = Resources.LoadAll<AudioClip>("Audio");
        foreach (AudioClip clip in clips)
        {
            soundMap.Add(clip.name, clip);
        }

    }

    public float GetGlobalVolume()
    {
        return GlobalVolume;
    }

    public float GetMusicVolume()
    {
        return MusicVolume;
    }

    public float GetSoundVolume()
    {
        return SoundVolume;
    }

    public void SetGlobalVolume(float volume)
    {
        GlobalVolume = volume;
        SetChannelVolume(SoundVolume * GlobalVolume, "SoundChannel");
        SetChannelVolume(MusicVolume * GlobalVolume, "MusicChannel");
    }

    public void SetSoundVolume(float volume)
    {
        SoundVolume = volume;
        SetChannelVolume(SoundVolume * GlobalVolume, "SoundChannel");
    }

    public void SetMusicVolume(float volume)
    {
        MusicVolume = volume;
        SetChannelVolume(MusicVolume * GlobalVolume, "MusicChannel");
    }

    private void SetChannelVolume(float volume, string channelName)
    {
        AudioSource[] channels = GameObject.FindObjectsOfType<AudioSource>();
        foreach (AudioSource source in channels)
        {
            if (source.tag == channelName)
            {
                source.volume = volume;
            }

        }
    }

    public void PlayMusic(string name)
    {
        if (!isMusicMuted)
        {
            musicChannel.clip = soundMap[name];
            musicChannel.volume = MusicVolume * GlobalVolume;
            musicChannel.Play();
        }
    }

    public void PlayMusicWithIntro(string introName, string loopName)
    {
        if (!isMusicMuted)
        {
            if(musicChannel.isPlaying)
            {
                musicDelayOverride = true;
            }
            PlayMusic(introName);
            StartCoroutine(PlayMusicDelayed(loopName, musicChannel.clip.length));
        }
    }

    private IEnumerator PlayMusicDelayed(string name, float delayTime)
    {
        if (!isMusicMuted)
        {
            yield return new WaitForSeconds(delayTime);
            if (musicDelayOverride)
            {
                musicDelayOverride = false;
            }
            else
            {
                PlayMusic(name);
            }
        }
    }

    public void PlaySound(string name)
    {
        if (!isSoundMuted)
        {
            AudioClip clip = soundMap[name];
            soundChannel.PlayOneShot(clip, GlobalVolume * SoundVolume);
        }
    }

    public void PlaySound(AudioClip clip)
    {
        if (!isSoundMuted)
        {
            soundChannel.PlayOneShot(clip, GlobalVolume * SoundVolume);
        }
    }

    public void PlaySound(AudioClip clip, float volume)
    {
        if (!isSoundMuted)
        {
            soundChannel.PlayOneShot(clip, volume * GlobalVolume * SoundVolume);
        }
    }

    public void PlaySound(string name, float volume)
    {
        if (!isSoundMuted)
        {
            soundChannel.PlayOneShot(soundMap[name], volume * GlobalVolume * SoundVolume);
        }
    }

    public void StopSound()
    {
        soundChannel.Stop();
    }

    public void ToggleSoundMute()
    {
        isSoundMuted = !isSoundMuted;
        ToggleMute(isSoundMuted, "SoundChannel");
    }

    public void ToggleMusicMute()
    {
        isMusicMuted = !isMusicMuted;
        ToggleMute(isMusicMuted, "MusicChannel");
    }

    private void ToggleMute(bool isMuted, string channelName)
    {
        AudioSource[] channels = GameObject.FindObjectsOfType<AudioSource>();
        foreach (AudioSource source in channels)
        {
            if (isMuted && source.name == channelName && source.isPlaying)
            {
                source.Stop();
            }
        }
    }

    public void ToggleAudioMute()
    {
        if (audioMute)
        {
            AudioListener.volume = 0;
            audioMute = true;
        }
        else
        {
            AudioListener.volume = 1;
            audioMute = false;
        }
    }

}
