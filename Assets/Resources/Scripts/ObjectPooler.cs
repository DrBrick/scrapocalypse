﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPoolItem
{
    public GameObject ObjectToPool;
    public int AmountToPool;
    public bool ShouldExpand = true;

    public ObjectPoolItem(GameObject _objectToPool, int _amountToPool, bool _shouldExpand)
    {
        ObjectToPool = _objectToPool;
        AmountToPool = _amountToPool;
        ShouldExpand = _shouldExpand;
    }
}

public class ObjectPooler : Singleton<ObjectPooler>
{
    public List<ObjectPoolItem> itemsToPool;
    public List<GameObject> pooledObjects;

    void Start()
    {
        Init();
    }

    protected override void Init()
    {
        pooledObjects = new List<GameObject>();
        itemsToPool = new List<ObjectPoolItem>();
        foreach (ObjectPoolItem item in itemsToPool)
        {
            Debug.Log(item);
            for (int i = 0; i < item.AmountToPool; i++)
            {
                GameObject obj = Instantiate(item.ObjectToPool);
                obj.SetActive(false);
                pooledObjects.Add(obj);
            }
        }

    }

    public void PoolObject(ObjectPoolItem _objectPoolItem)
    {
        pooledObjects = new List<GameObject>();

        for (int i = 0; i < _objectPoolItem.AmountToPool; i++)
        {
            GameObject obj = Instantiate(_objectPoolItem.ObjectToPool);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }
    }

    public GameObject GetPooledObject(string tag)
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy && pooledObjects[i].tag == tag)
            {
                return pooledObjects[i];
            }
        }
        foreach (ObjectPoolItem item in itemsToPool)
        {
            if (item.ObjectToPool.tag == tag)
            {
                if (item.ShouldExpand)
                {
                    GameObject obj = Instantiate(item.ObjectToPool);
                    obj.SetActive(false);
                    pooledObjects.Add(obj);
                    return obj;
                }
            }
        }
        Debug.Log("Tried to grab too many objects from non-expanding pool");
        return null;
    }
}
