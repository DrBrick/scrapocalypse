﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HordeMode : MonoBehaviour {
    
    public Timer timer;
    public float introTimerDelay;
    public float waveDuration;
    public float individualSpawnDelay;
    public GameObject enemyInstance;
    public GameObject enemies;
    public GameObject spawnPoints;
    public GameObject hordeIntro;
    public GameObject waveIntro;
    public Text score;

	// Use this for initialization
	void Start () {
        foreach(Transform t in spawnPoints.transform)
        {
            t.GetComponent<SpriteRenderer>().enabled = false;
        }
        waveIntro.GetComponent<Fader>().SetAlphaAll(0.0f);
        hordeIntro.GetComponent<Fader>().SetAlphaAll(0.0f);
        hordeIntro.GetComponent<Fader>().CycleShow();
        timer.duration = introTimerDelay;
        timer.StartTimer();
	}
	
	// Update is called once per frame
	void Update () {
		if(timer.IsTick())
        {
            StartNextWave();
        }

        score.text = "Score " + GameManager.Instance.score;
	}

    public void StartNextWave()
    {
        if(GameManager.Instance.waveCount >0)
        {
            GameManager.Instance.score += 100 * GameManager.Instance.waveCount;
        }
        GameManager.Instance.waveCount += 1;
        waveIntro.transform.Find("TextWave").GetComponent<Text>().text = "Wave: " +
            GameManager.Instance.waveCount.ToString();
        waveIntro.GetComponent<Fader>().CycleShow();
        timer.Reset();
        timer.duration = waveDuration + GameManager.Instance.waveCount;
        timer.StartTimer();
        StartCoroutine(SpawnWave());
    }

    //public void SpawnWave()
    //{
    //    int randomInt = 0;
    //    for(int i = 0; i<waveCount; i++)
    //    {
    //        GameObject go = Instantiate<GameObject>(enemyInstance, enemies.transform);
    //        randomInt = Random.Range(0, spawnPoints.transform.childCount);
    //        go.transform.position = spawnPoints.transform.GetChild(randomInt).position;

    //    }
    //}

    IEnumerator SpawnWave()
    {
        int count = 0;
        int randomInt = 0;
        do
        {
            count += 1;
            //GameObject go = Instantiate<GameObject>(enemyInstance, enemies.transform);
            GameObject go = ObjectPoolManager.Instance.GetObjectFromPool("Shockbot");
            randomInt = Random.Range(0, spawnPoints.transform.childCount);
            go.transform.position = spawnPoints.transform.GetChild(randomInt).position;
            go.SetActive(true);
            go.GetComponent<Health>().Initialize();
            yield return new WaitForSeconds(individualSpawnDelay);
        } while (count <= GameManager.Instance.waveCount);
        
    }
}
