﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public float RotationOffset = 0f;
    Player player;

    float h, v;

    void Start()
    {
        player = GetComponent<Player>();
    }

    // TODO: Make controls configurable - use axes, not hard coded keys
    void Update()
    {
        ////Run
        //if (Input.GetKeyDown(KeyCode.LeftShift))
        //{
        //    player.OnRunInputDown();
        //}
        //if (Input.GetKeyUp(KeyCode.LeftShift))
        //{
        //    player.OnRunInputUp();
        //}

        //Fire
        if (Input.GetMouseButtonDown(0))
        {
            player.OnFireButtonDown();
        }
        if (Input.GetMouseButtonUp(0))
        {
            player.OnFireButtonUp();
        }

        ////Reload
        //if (Input.GetKeyDown(KeyCode.R))
        //{
        //    player.OnReloadButtonDown();
        //}

        // Store the input axes.
        //h = Input.GetAxisRaw("Horizontal");
        //v = Input.GetAxisRaw("Vertical");
        player.DirectionalInput(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        //rotate weapon
        //player.weapon.transform.right = GetMousePosition();
        var mousePos = GetMousePosition();
        var direction = mousePos - (Vector2)transform.position;
        //if (player.Weapon)
        //{
        //    player.Weapon.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg +  RotationOffset));
        //}
        //transform.rotation = Quaternion.Euler(Vector3(0, 0, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg));
        player.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg + RotationOffset));

    }
    
    //void FixedUpdate()
    //{
    //    player.Move(h, v);
    //}

    private Vector2 GetMousePosition()
    {
        Vector2 mousePos = Input.mousePosition;
        var c = Camera.main;
        var p = new Vector2();

        //p = c.orthographic ? c.ScreenToWorldPoint(mousePos) : GetWorldPositionOnPlane(mousePos);
        if (c.orthographic)
        {
            p = c.ScreenToWorldPoint(mousePos);
        }
        else
        {
            Debug.Log("Sorry, only orthographic camera for now pls");
            ////for perspective view, because it's difficult like that
            //Ray ray = Camera.main.ScreenPointToRay(mousePos);
            ////modify z dynamically if game ever leaves z.0
            ////Plane plane = new Plane(Vector2.forward, new Vector2(0, 0, z));
            //Plane plane = new Plane(Vector2.forward, new Vector2(0, 0, 0));
            //float distance;
            //plane.Raycast(ray, out distance);
            //p = ray.GetPoint(distance);
            ////p = ray.GetPoint(range);
            //p.z = 0;
        }
        return p;
    }
    
}
